import React, { Component } from 'react';
import './help.css'

class Help extends Component {
    render() { 
        return (
            <div className="container">
                <div class="row">
                    <div className="col-lg-12 col-md-12 colsm-12">
                        <p className="text-center small-heading">How we help</p>
                    </div>
                    <div className="col-lg-6 col-md-12 col-sm-12 image">
                        <img src="https://curai.com/Patient1Compressed.jpg" alt=""/>
                    </div>
                    <div className="col-lg-4 col-md-12 col-sm-12 right-section">
                        <p className="section-heading">Empowering Patients</p>
                        <p className="section-text">Curai puts patients at the center of care: assisted conversations help the patient describe symptoms, prompt the answers a doctor needs to make an accurate diagnosis, and offer information to ensure the patient understands their condition and treatment options.</p>
                        <svg width="95" height="5"><path d="M2.245 2.5H92" stroke="#FF594D" stroke-width="4.2" fill="none" fill-rule="evenodd" stroke-linecap="square"></path></svg>
                    </div>
                    <div className="col-lg-12 col-md-12 col-sm-12 mt-5  ">
                    <svg width="83" height="170" style={{margin: "-2.3rem 115px -20px -33px"}}><path d="M2 0v89.965C2 121.645 21.635 169 82 168.19" stroke="#272727" stroke-width="2.2" fill="none" fill-rule="evenodd"></path></svg>
                    </div>
                    <div className="col-lg-5 col-md-12 col-sm-12 offset-lg-2 left-section">
                    <p className="section-heading">Augmenting doctors</p>
                        <p className="section-text">We’re helping doctors focus on doctoring. Smart charting pulls together a patient’s history to ensure the doctor has context, and augmented diagnostics help to translate symptoms into potential conditions. So doctors can spend time on what matters most: treating patients</p>
                        <svg width="95" height="5"><path d="M2.245 2.5H92" stroke="#FF594D" stroke-width="4.2" fill="none" fill-rule="evenodd" stroke-linecap="square"></path></svg>
                    </div>
                    <div className="col-lg-5 col-md-12 col-sm-12 image-left">
                    <img src="https://curai.com/Doctor.jpg" alt=""/>  
                    </div>
                    <div className="col-lg-12 text-center">
                    <svg width="83" height="170" style={{margin: "10px 115px -20px -33px"}}><path d="M2 0v89.965C2 121.645 21.635 169 82 168.19" stroke="#272727" stroke-width="2.2" fill="none" fill-rule="evenodd"></path></svg>
                    <p className="small-heading text-center">Opening access</p>
                     </div>
                    
                </div>
            </div>
          );
    }
}
 
export default Help;