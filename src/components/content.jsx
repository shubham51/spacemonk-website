import React, { Component } from "react";
import "./content.css";

class Content extends Component {
  render() {
    return (
      <div className="container-fluid content">
          <div className="row">
            <div className="col-lg-8 col-sm-10 offset-lg-2 col-md-12">
                                    <h1 className="col-sm-offset-5 mr-4 big-heading">Providing the world's best healthcare to everyone</h1>

            </div>
            <div className="col-lg-6 col-sm-12 col-md-12 offset-lg-3">
                <p className="small-heading ">We are harnessing AI/ML to build products that empower both providers and patients.</p>
            </div>
            <div className="col-lg-2 col-md-2 col-sm-2"></div>
          </div>
      </div>
    );
  }
}

export default Content;

