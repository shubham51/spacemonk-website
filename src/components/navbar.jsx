import React, { Component } from "react";
import "./navbar.css";

class Navbar extends Component {
  open() {
    console.log("hello");
    document.getElementById("myNav").style.width = "100%";
    document.getElementById("myNav").style.background = "white";

  }
  close() {
    document.getElementById("myNav").style.width = "0%";
    document.getElementById("myNav").style.background = "white";
  }

  render() {
    return (
      <nav className="navbar navbar-expand-lg sticky-top ">
        <div id="myNav" class="overlay">
          <a href="javascript:void(0)" class="closebtn" onClick={this.close} style={{color:"red"}}>
            &times;
          </a>

          <div class="overlay-content">
            <a href="#">WORKING HERE</a>
            <a href="#">FIRST OPTION</a>
          </div>
        </div>

        <a className="navbar-brand offset-md-5" href="/">
          Curai
        </a>
        <button
          className="navbar-toggler"
          type="button"
          onClick={this.open}
        >
          <span className="navbar-toggler-icon">
            <i
              className="fas fa-bars"
              style={{ color: "red", fontSize: "28px;" }}
            ></i>
          </span>
        </button>

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav ml-auto">
            <li className="nav-item">
              <a className="nav-link working" href="#">
                WORKING HERE
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link rectangle" href="#">
                First option
              </a>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}

export default Navbar;
