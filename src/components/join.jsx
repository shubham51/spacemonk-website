import React, { Component } from 'react';
import "./join.css"

class Join extends Component {
    render() { 
        return ( 
            <div className="join">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-9 offset-lg-2">
                            <p className="join-text">
                            We are building a diverse, world-class, multi-disciplinary team across engineering, AI research, design, and medicine.
                            </p>
                        </div>
                        <div className="text-center join-heading col-lg-10 offset-lg-1">
                            <h1>Join Us.</h1>
                        </div>
                        <div className="col-lg-10 offset-lg-1 text-center join-a">
                            <a href="#">work at curai</a>
                        </div>
                    </div>
                </div>
            </div>
         );
    }
}
 
export default Join;