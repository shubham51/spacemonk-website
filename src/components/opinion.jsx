import React, { Component } from "react";
import "./opinion.css";
class Opinion extends Component {
  render() {
    return (
      <div className="row opinion">
        <div className="col-lg-6 col-md-12 col-sm-12 opinion-box">
          <img
            src="https://curai.com/FOLogo.png"
            loading="eager"
            alt="First Opinion Logo"
            img2x="/FOLogo.png"
            style={{ height: "50px" }}
          />
          <div className="col-lg-12 col-md-12 col-sm-12 opinion-text">
            Curai’s technology drives First Opinion, the fastest way to chat
            with a doctor for free. We are building the future of healthcare by
            creating innovative product experiences to deliver services that are
            easy and convenient to use.
          </div>
          <button type="button" className="btn more-button">Find out more &nbsp;
          <svg width="40" height="11"><path d="M18.805 6.322l-4.35 4.323c-.62.617-1.583-.342-.962-.957 1.055-1.05 2.11-2.099 3.167-3.148H1.45c-.879 0-.877-1.353 0-1.353H16.7L13.493 2c-.62-.618.341-1.574.963-.957 1.449 1.44 2.9 2.88 4.349 4.323a.683.683 0 010 .956" fill="#FF594D" fill-rule="evenodd"></path></svg></button>
        </div>
        <div className="col-lg-6 col-md-12 col-sm-12 opinion-image">
        <img src="https://curai.com/Patient2.jpg" loading="eager" alt="Fastest way to chat with Doctor" img2x="/Patient2.jpg" />
        </div>
      </div>
    );
  }
}

export default Opinion;
