import React, { Component } from 'react';
import "./blob.css"
class Blob extends Component {
    render() { 
        return ( 
                <div className="row">
                    <div className="col-md-3 large">
                        <img src="https://curai.com/HomeUpperLeftBlob.png" alt=""/>
                    </div>
                    <div className="col-lg-6 col-sm-12">
                    <img src="https://curai.com/hero_illustration_home.png" className="cartoon-img" loading="eager" alt="Curai's Mission" />
                    </div>
                    <div className="col-md-3 large">
                    <img src="https://curai.com/HomeUpperRightBlob.png"style={{marginLeft:"2.8rem"}} alt=""/>
                    </div> 
                </div>
                
         );
    }
}
 
export default Blob;