import React from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.css";
import "../node_modules/jquery/dist/jquery.min.js";
import "bootstrap/dist/js/bootstrap.min.js";
import Navbar from "./components/navbar";
import Content from "./components/content";
import Blob from "./components/blob";
import Help from "./components/help";
import Opinion from "./components/opinion";
import Platform from "./components/platform";
import Join from "./components/join";

function App() {
  return (
    <div className="App">
      <Navbar />
      <Content />
      <Blob />
      <Help />
      <Opinion />
      <Platform />
      <Join />
    </div>
  );
}

export default App;
